# Tepper-Math-Prep

## [Module 1 Notes - Functions and Graphs](module-1.md)

## [Module 2 Notes - Differentiation of Single Variable Functions](module-2.md)

## [Module 3 Notes - Optimization](module-3.md)

## [Module 4 Notes - Differentiation of Multiple Variable Functions and Integration](module-4.md)
