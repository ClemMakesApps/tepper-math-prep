# Examples

1. Find the derivative of $`f(x)=3^{7x}`$
1. Find the derivative of $`f(x)=(\ln(e^x+1))^\frac{1}{2}`$
