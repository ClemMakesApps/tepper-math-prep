## Module 1 Notes

### Quadratics

| |  |
|---|---|
| Solving for `y` aka $`f(x)`$ | $`f(x) = ax^2+bx+c`$  | 
| Solving for `x` | $`x = \frac{-b\pm\sqrt{b^2-4ac}}{2a}`$  |
|No solution in reals IF | $`b^2-4ac < 0`$  |
|One solution in reals IF | $`b^2-4ac = 0`$  |
|Two solutions in reals IF | $`b^2-4ac > 0`$  |

### Exponentials

| Function | Equals |
|---|---|
| $`a^0`$  | $`=1`$  | 
| $`a^{\frac{1}{5}}a^{\frac{4}{5}}`$  | $`=a`$  |
| $`a^xa^y`$  | $`=a^{x+y}`$  |
| $`\frac{a^x}{a^y}`$  | $`=a^{x-y}`$  |
| $`\frac{a^x}{a^y}`$  | $`=a^{x-y}`$  |
| $`(a^x)^y`$  | $`=a^xy`$  |
| $`(ab)^x`$  | $`=a^xb^x`$  |

### Logarithmics

> Note: For a base `b > 0` and positive real numbers `M`, `N` and real numbers `p` and `x`

| Function | Equals |
|---|---|
| `y` (when `b > 0` and `b != 1`) | $`y = \log_{b}x`$ |
| `x` (when `x` = positive real numbers) | $`x=b^y`$ |
| $`\log_{b}1`$ | 0 |
| $`\log_{b}b`$ | 1 |
| $`\log_{b}a`$ | $`\frac{\ln{a}}{\ln{b}}`$ when b != e |
| $`\log_{b}b^x`$ | $`x`$ |
| $`b\log_{b}x`$ | $`x`$ |
| $`\log_{b}MN`$ | $`\log_{b}M + \log_{b}N`$ |
| $`\log_{b}\frac{M}{N}`$ | $`\log_{b}M - \log_{b}N`$ |
| $`\log_{b}M^p`$ | $`p\log_{b}M`$ |

### Continuous Growth

| Function | Equals |
|---|---|
| Compound Interest | $`A = P (1 + \frac{r}{m})^{mt}`$ |
| Continuous Interest | $`A = Pe^{rt}`$ |

Where `A` is the future value at the end of `t` years
- `P` is the current Principal
- `r` is the annual rate
- `m` is the number of compounding periods per year
- `t` time in years
